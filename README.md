# erpcdemo

基于串口通信的RPC调用实现(demo)

## 环境要求

|ubuntu 16.04|
|:--|
|gcc > 5.2.0|

## 代码下载

	git clone --recursive  https://gitee.com/l0km/erpcdemo.git

**NOTE:** 加了 --recursive 选项才会下载关联的子项目eprc


## 编译准备

安装依赖库

	# 安装跨平台编译工具 cmake
	sudo apt-get install cmake
	# install flex & bison
	sudo apt-get install flex bison
	# install boost
	sudo apt-get install libboost-dev libboost-system-dev libboost-filesystem-dev

## erpcgen:IDL编译器

erpcgen是IDL编译器，用于将`.erpc`后缀的接口定义(IDL)文件生成对应的client/server代码。

进入[erpc](erpc)子项目编译erpc编译器并安装到`/usr/local/bin`下: 

	cd erpc/erpcgen
	make -j8
	sudo make install 

**NOTE:** `-j8` 为并行编译选项，指定使用8个线程同时编译，以加快编译速度

## eprc

执行　['install_erpc.sh'](install_erpc.sh)编译erpc核心库并安装到release文件夹下

## 创建eclipse工程(linux)

执行[`make_unix_makefile.sh`](make_unix_makefile.sh)脚本，会在当前项目文件夹所在文件夹下生成同级的eclipse项目文件(erpc.gcc)

## 命令行编译
	
编译上一步生成的Makefile，安装到 release 文件夹下,也可以直接执行[`build.sh`](build.sh)

	cmake --build ../erpcdemo.gcc --target install

## IDL定义[TODO]

[erpcdemo.erpc](erpcdemo.erpc)为人脸锁的RPC调用接口定义文件(interface description language)，根据项目需要添加相关的定义.

IDL语法参见erpc官方文档:

[《IDL Reference(https://github.com/EmbeddedRPC/erpc/wiki/IDL-Reference)》](https://github.com/EmbeddedRPC/erpc/wiki/IDL-Reference)

## 生成代码

根据接口定义文件[erpcdemo.erpc](erpcdemo.erpc)生成对应的client/server代码

	erpcgen erpcdemo.erpc

**NOTE:** 事前必须先执行erpcgen编译安装。

生成文件列表：
	
	erpcdemo.h
	erpcdemo_client.cpp
	erpcdemo_server.cpp
	erpcdemo_server.h
