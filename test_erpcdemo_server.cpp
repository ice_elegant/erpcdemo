/*
 * test_erpcdemo_server.cpp
 *
 *  Created on: Apr 15, 2020
 *      Author: guyadong
 */

#include <iostream>
#include <erpc_server_setup.h>
#include "erpcdemo_server.h"
#include "erpc_setup_tcp.h"
using namespace std;
int main(int argc, char *argv[]){
	// 创建client端传输层对象(TCP),127.0.0.1:5407
	auto transport = erpc_transport_tcp_init("127.0.0.1",5407, true);
	/* init eRPC server environment */
	/* MessageBufferFactory initialization */
	erpc_mbf_t message_buffer_factory = erpc_mbf_dynamic_init();
	/* eRPC server side initialization */
	erpc_server_init(transport, message_buffer_factory);
	/* connect generated service into server, look into erpcdemo_server.h */
	erpc_add_service_to_server(create_DEMO_service());
	cout << "start erpcdemo server" << endl;
	/* run server */
	erpc_server_run(); /* or erpc_server_poll(); */
	/* 关闭socket */
	erpc_transport_tcp_deinit();
	return 0;
}
