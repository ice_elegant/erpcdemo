/*
 * test_erpcdemo_client.c
 *
 *  Created on: Apr 14, 2020
 *      Author: guyadong
 */


#include <string.h>
#include <iostream>
#include <erpc_client_setup.h>
#include <erpc_port.h>
#include "erpcdemo.h"
#include "erpc_setup_tcp.h"
using namespace std;
// Free space allocated inside struct binary_t function implementation
static void free_binary_t_struct(binary_t * data)
{
    if (data->data)
    {
        erpc_free(data->data);
    }
}
int main(int argc, char *argv[])
{
	/* 创建client端传输层对象(TCP),127.0.0.1:5407 */
	auto transport = erpc_transport_tcp_init("127.0.0.1",5407, false);
	auto message_buffer_factory = erpc_mbf_dynamic_init();
	/* 初始化客户端 */
	erpc_client_init(transport,message_buffer_factory);
	auto msg = "hello!!!!!";
	binary_t b = {(uint8_t*)msg,(uint32_t)strlen(msg)};
	{
		/* RPC 调用 */
		auto resp = RD_demoHello(&b);
		/* 输出返回值 */
		cout << "RD_demoHello response:" << resp->data << endl;
		/* 对于返回指针类型的数据,用完后需要释放RD_demoHello中分配的内存 */
		free_binary_t_struct(resp);
	}
	{
		binary_t r;
		auto code = RD_demoHello2(&b,&r);
		if(code==lErrorOk_c){
			/* 输出返回值 */
			cout << "RD_demoHello2 response:" << r.data << endl;
			/* 用完后需要释放RD_demoHello2中分配的内存 */
			erpc_free(r.data);
		}
		else{
			cout << "RD_demoHello response ERROR: " << code << endl;
		}
	}
	{
		int8_t out[256];
		int32_t size;
		auto code = RD_demoHello3(&b,out,&size);
		if(code==lErrorOk_c){
			/* 输出返回值 */
			string out_str(out,out+size);
			cout << "RD_demoHello3 response:" << out_str << endl;
		}
		else{
			cout << "RD_demoHello response ERROR: " << code << endl;
		}
	}
	/* 关闭socket */
	erpc_transport_tcp_deinit();
}
