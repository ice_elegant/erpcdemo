#!/bin/bash
# 创建linux下的eclipse工程
# 要求编译器支持C++11
[ `g++ -dumpversion` \< "4.8.0" ] && echo "g++ compiler required version 4.8.0 or above" && exit -1

sh_folder=$(dirname $(readlink -f $0))
folder_name=$(basename $sh_folder) 
# 定义编译的版本类型(DEBUG|RELEASE)
build_type=DEBUG
typeset -u arg1=$1
[ "$arg1" = "DEBUG" ] && build_type=$arg1
echo build_type=$build_type

pushd $sh_folder/..

[ -d $folder_name.gcc ] && rm -fr $folder_name.gcc
mkdir $folder_name.gcc

pushd $folder_name.gcc
echo "creating linux Eclipse Project for gcc ..."

ERPC_INSTALL_FOLDER=$sh_folder/release/erpc_$(uname -s)_$(uname -m)
# erpc库必须先编译出来，否则报错退出
[ ! -d $ERPC_INSTALL_FOLDER ] \
	&& echo "not found $ERPC_INSTALL_FOLDER" \
	&& echo "must run install_erpc.sh firstly" \
	&& echo "请先执行　install_erpc.sh　生成erpc核心库" \
	&& exit -1

cmake "$sh_folder" -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=$build_type \
	-DCMAKE_INSTALL_PREFIX=$sh_folder/release/erpcdemo_$(uname -s)_$(uname -m) \
	-DCMAKE_PREFIX_PATH=$ERPC_INSTALL_FOLDER 

popd
popd

