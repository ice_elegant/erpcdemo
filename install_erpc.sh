#!/bin/bash
# 生成erpc核心库并安装到当前文件夹下
sh_folder=$(dirname $(readlink -f $0))
folder_name=$(basename $sh_folder) 
pushd $sh_folder/erpc || exit -1
make clean || exit -1
cd erpc_c
make -j8 || exit -1
# 安装到当前文件夹下的release目录
make install PREFIX=$sh_folder/release/erpc_$(uname -s)_$(uname -m)
popd
