/*
 * erpcdemo_server_impl.cpp
 * erpcdemo RPC implementation
 *  Created on: Apr 15, 2020
 *      Author: guyadong
 */

#include <iostream>
#include <time.h>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <string.h>
#include "erpcdemo_server.h"
using namespace std;
/** 返回当前时间字符串 */
static std::string now_str() {
	time_t t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::stringstream ss;
	ss << std::put_time(std::localtime(&t), "%F %T");
	return ss.str();
}

binary_t * RD_demoHello(const binary_t * txInput){
	cout << "RD_demoHello called" << endl;
	string o ((char*)txInput->data);
	o.append("@").append(now_str());
	auto ol = strlen(o.c_str());
	char* buf = (char*)malloc(ol + 1);
	strncpy(buf,o.c_str(),ol);
	return new binary_t{(uint8_t*)buf,(uint32_t)ol};
}

lockErrors_t RD_demoHello2(const binary_t * txInput, binary_t * txOutput)
{
	cout << "RD_demoHello2 called" << endl;
	string o ((char*)txInput->data);
	o.append("@").append(now_str());
	auto ol = strlen(o.c_str());
	char* buf = (char*)malloc(ol + 1);
	if(!buf){
		return lErrorOutofMemory_c;
	}
	strncpy(buf,o.c_str(),ol);
	txOutput->data = (uint8_t*)buf;
	txOutput->dataLength = (uint32_t)ol;
	return lErrorOk_c;
}
lockErrors_t RD_demoHello3(const binary_t * txInput, int8_t txOutput[64], int32_t * size)
{
	cout << "RD_demoHello3 called" << endl;
	string o ((char*)txInput->data);
	o.append("@").append(now_str());
	strncpy((char*)txOutput,o.c_str(),64);
	*size = (int32_t)o.size();
	return lErrorOk_c;
}
